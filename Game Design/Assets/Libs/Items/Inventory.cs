﻿using System;
using System.Collections.Generic;
using UnityEditor;

namespace Assets.Libs.Items
{
    public class Inventory
    {
        public Dictionary<ItemType, HashSet<IBaseItem>> Items { get; }
        public float VolumeCapacity { get; }
        public float VolumeUsed { get; private set; }
        public float InventoryWeight { get; private set; }

        public Inventory(float capacity)
        {
            VolumeCapacity = capacity;
            
            Items = new Dictionary<ItemType, HashSet<IBaseItem>>();
            foreach (var value in Enum.GetValues(typeof(ItemType)))
            {
                Items.TryAdd((ItemType)value, new HashSet<IBaseItem>());
            }

            VolumeUsed = 0;
            InventoryWeight = 0;
        }

        public bool StoreItem(IBaseItem item)
        {
            if (VolumeCapacity < VolumeUsed + item.Volume) return false;
            
            if (!Items[item.Type].Add(item)) return false;


            VolumeUsed += item.Volume;
            InventoryWeight += item.Weight;

            return true;

        }

        public bool RemoveItem(IBaseItem item)
        {
            if (!Items[item.Type].Remove(item)) return false;


            VolumeUsed -= item.Volume;
            InventoryWeight -= item.Weight;

            return true;

        }
    }
}