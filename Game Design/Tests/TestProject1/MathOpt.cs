﻿namespace TestQuadTree
{
    public static class MathOpt
    {
        public static long GetDistance(Tuple<long, long> position1, Tuple<long, long> position2)
        {
            var xDistance = position1.Item1 - position2.Item1;
            var yDistance = position1.Item2 - position2.Item2;

            var distance = xDistance * xDistance + yDistance * yDistance;

            if (0 == distance) { return 0; }  
            long n = (distance / 2) + 1;      
            long n1 = (n + (distance / n)) / 2;
            while (n1 < n)
            {
                n = n1;
                n1 = (n + (distance / n)) / 2;
            }

            return n;
        }

        public static bool BoundingBoxesIntersect(
            Tuple<long, long> position1, long radius1,
            Tuple<long, long> position2, long radius2)
        {
            if (position1 == null || position2 == null 
                || (radius1 == 0 && radius2 == 0)) { return false; }

            var xDistance = Math.Abs(position2.Item1 - position1.Item1);
            var yDistance = Math.Abs(position2.Item2 - position1.Item2);

            return xDistance <= radius1 + radius2 && yDistance <= radius1 + radius2;
        }


    }
}