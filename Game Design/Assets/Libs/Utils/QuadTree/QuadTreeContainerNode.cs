﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JetBrains.Annotations;

namespace Assets.Libs.Utils.QuadTree
{

    public class QuadTreeContainerNode<TNodeData> where TNodeData : IQuadTreeKey
    {
        [CanBeNull] private QuadTreeContainerNode<TNodeData> _parentNode;

        [CanBeNull] private QuadTreeContainerNode<TNodeData> _upLeftNode;
        [CanBeNull] private QuadTreeContainerNode<TNodeData> _upRightNode;
        [CanBeNull] private QuadTreeContainerNode<TNodeData> _downLeftNode;
        [CanBeNull] private QuadTreeContainerNode<TNodeData> _downRightNode;

        public readonly Tuple<long, long> CenterPosition;
        public readonly long Radius;
        public readonly int RecursionDepth;

        [CanBeNull] public TNodeData Data;


        private QuadTreeContainerNode([CanBeNull] QuadTreeContainerNode<TNodeData> parentNode, Tuple<long, long> centerPosition,
            long radius, int recursionDepth)
        {
            _parentNode = parentNode;
            CenterPosition = centerPosition;
            Radius = radius;
            RecursionDepth = recursionDepth;
        }

        [CanBeNull]
        private QuadTreeContainerNode<TNodeData> GetNode(Tuple<long, long> position)
        {
            if (_upLeftNode == null) return this;


            if (position.Item1 >= CenterPosition.Item1)
            {
                if (position.Item2 >= CenterPosition.Item2)
                {
                    return _upRightNode?.GetNode(position);
                }
                else
                {
                    return _downRightNode?.GetNode(position);
                }
            }
            else
            {
                if (position.Item2 >= CenterPosition.Item2)
                {

                    return _upRightNode?.GetNode(position);
                }
                else
                {
                    return _downLeftNode?.GetNode(position);
                }
            }
        }

        #region Adding/Removing


        public bool StoreData(TNodeData data)
        {
            if (Data == null && _upLeftNode == null)
            {
                Data = data;
                return true;
            }

            if (Radius > 0 && Data != null)
            {
                CutInFour();

                var oldData = Data;
                Data = default;

                if (!StoreData(oldData)) throw new Exception($"Error while trying to store {Data}");


                return StoreData(data);

            }

            if (Radius == 0 && Data != null) return false;


            if (data.Position.Item1 >= CenterPosition.Item1)
            {
                return data.Position.Item2 >= CenterPosition.Item2
                    ? _upRightNode!.StoreData(data)
                    : _downRightNode!.StoreData(data);
            }
            else
            {
                return data.Position.Item2 >= CenterPosition.Item2
                    ? _upLeftNode!.StoreData(data)
                    : _downLeftNode!.StoreData(data);
            }
        }

        [CanBeNull]
        public TNodeData RemoveDataAtPosition(Tuple<long, long> position)
        {

            if (Data != null)
            {
                var data = Data;
                Data = default;
                SimplifyTree();
                return data;
            }

            if (position.Item1 >= CenterPosition.Item1)
            {
                if (position.Item2 >= CenterPosition.Item2)
                {
                    return _upRightNode == null ? default : _upRightNode.RemoveDataAtPosition(position);
                }
                else
                {
                    return _downRightNode == null ? default : _downRightNode.RemoveDataAtPosition(position);
                }
            }
            else
            {
                if (position.Item2 >= CenterPosition.Item2)
                {

                    return _upLeftNode == null ? default : _upLeftNode.RemoveDataAtPosition(position);
                }
                else
                {
                    return _downLeftNode == null ? default : _downLeftNode.RemoveDataAtPosition(position);
                }
            }
        }

        #endregion

        #region TreeBuilding

        public static QuadTreeContainerNode<TNodeData> CreateRootNode()
        {
            var rootNode = new QuadTreeContainerNode<TNodeData>(default, new Tuple<long, long>(0, 0), 1, 1);

            rootNode._upLeftNode = new QuadTreeContainerNode<TNodeData>(rootNode, new Tuple<long, long>(-1, 0), 0, 0);
            rootNode._upRightNode = new QuadTreeContainerNode<TNodeData>(rootNode, new Tuple<long, long>(0, 0), 0, 0);
            rootNode._downLeftNode =
                new QuadTreeContainerNode<TNodeData>(rootNode, new Tuple<long, long>(-1, -1), 0, 0);
            rootNode._downRightNode =
                new QuadTreeContainerNode<TNodeData>(rootNode, new Tuple<long, long>(0, -1), 0, 0);

            return rootNode;
        }

        public QuadTreeContainerNode<TNodeData> IncreaseRootDepth()
        {
            var newRoot = new QuadTreeContainerNode<TNodeData>(null, CenterPosition, Radius * 2, RecursionDepth + 1);

            newRoot.CutInFour();



            newRoot._upRightNode!.CutInFour();
            newRoot._upRightNode!._downLeftNode = _upRightNode;
            _upRightNode!._parentNode = newRoot._upRightNode;
            //_upRightNode.SimplifyTree();


            newRoot._upLeftNode!.CutInFour();
            newRoot._upLeftNode._downRightNode = _upLeftNode;
            _upLeftNode!._parentNode = newRoot._upLeftNode;
            //_upLeftNode.SimplifyTree();

            newRoot._downLeftNode!.CutInFour();
            newRoot._downLeftNode._upRightNode = _downLeftNode;
            _downLeftNode!._parentNode = newRoot._downLeftNode;
            //_downLeftNode.SimplifyTree();

            newRoot._downRightNode!.CutInFour();
            newRoot._downRightNode._upLeftNode = _downRightNode;
            _downRightNode!._parentNode = newRoot._downRightNode;
            //_downRightNode.SimplifyTree();

            return newRoot;
        }

        private void SimplifyTree()
        {
            if (_parentNode == null) return;
            if (_parentNode._upLeftNode!.Data != null || _parentNode._upRightNode!.Data != null ||
                _parentNode._downRightNode!.Data != null || _parentNode._downLeftNode!.Data != null ||
                _parentNode._upLeftNode._upLeftNode != null || _parentNode._upRightNode._upLeftNode != null ||
                _parentNode._downRightNode._upLeftNode != null || _parentNode._downLeftNode._upLeftNode != null) return;

            _parentNode._upLeftNode = null;
            _parentNode._upRightNode = null;
            _parentNode._downRightNode = null;
            _parentNode._downLeftNode = null;

            _parentNode.SimplifyTree();
        }

        private bool CutInFour()
        {

            if (Radius == 0) return false;
            if (Radius == 1)
            {
                _upLeftNode = new QuadTreeContainerNode<TNodeData>(this,
                    new Tuple<long, long>((CenterPosition.Item1 - Radius), (CenterPosition.Item2)), (Radius / 2),
                    RecursionDepth - 1);
                _upRightNode = new QuadTreeContainerNode<TNodeData>(this,
                    new Tuple<long, long>((CenterPosition.Item1), (CenterPosition.Item2)), (Radius / 2),
                    RecursionDepth - 1);
                _downLeftNode = new QuadTreeContainerNode<TNodeData>(this,
                    new Tuple<long, long>((CenterPosition.Item1 - Radius), (CenterPosition.Item2 - Radius)),
                    (Radius / 2), RecursionDepth - 1);
                _downRightNode = new QuadTreeContainerNode<TNodeData>(this,
                    new Tuple<long, long>((CenterPosition.Item1), (CenterPosition.Item2 - Radius)), (Radius / 2),
                    RecursionDepth - 1);
                // Console.Out.WriteLine($"{RecursionDepth} : {Radius}  : {CenterPosition}   : {new Tuple<long, long>((CenterPosition.Item1 - Radius), (CenterPosition.Item2))}");
                // Console.Out.WriteLine($"{RecursionDepth} : {Radius}  : {CenterPosition}   : {new Tuple<long, long>((CenterPosition.Item1), (CenterPosition.Item2))}");
                // Console.Out.WriteLine($"{RecursionDepth} : {Radius}  : {CenterPosition}   : {new Tuple<long, long>((CenterPosition.Item1 - Radius), (CenterPosition.Item2 - Radius))}");
                // Console.Out.WriteLine($"{RecursionDepth} : {Radius}  : {CenterPosition}   : {new Tuple<long, long>((CenterPosition.Item1), (CenterPosition.Item2 - Radius))}");

                return true;
            }

            _upLeftNode = new QuadTreeContainerNode<TNodeData>(this,
                new Tuple<long, long>((CenterPosition.Item1 - (Radius / 2)), (CenterPosition.Item2 + (Radius / 2))),
                (Radius / 2), RecursionDepth - 1);
            _upRightNode = new QuadTreeContainerNode<TNodeData>(this,
                new Tuple<long, long>((CenterPosition.Item1 + (Radius / 2)), (CenterPosition.Item2 + (Radius / 2))),
                (Radius / 2), RecursionDepth - 1);
            _downLeftNode = new QuadTreeContainerNode<TNodeData>(this,
                new Tuple<long, long>((CenterPosition.Item1 - (Radius / 2)), (CenterPosition.Item2 - (Radius / 2))),
                (Radius / 2), RecursionDepth - 1);
            _downRightNode = new QuadTreeContainerNode<TNodeData>(this,
                new Tuple<long, long>((CenterPosition.Item1 + (Radius / 2)), (CenterPosition.Item2 - (Radius / 2))),
                (Radius / 2), RecursionDepth - 1);
            //Console.Out.WriteLine($"{RecursionDepth} : {Radius}  : {CenterPosition}  : {new Tuple<long, long>((CenterPosition.Item1 + (Radius / 2)), (CenterPosition.Item2 + (Radius / 2)))}");
            //Console.Out.WriteLine($"{RecursionDepth} : {Radius} : {CenterPosition} : {new Tuple<long, long>((CenterPosition.Item1 - (Radius / 2)), (CenterPosition.Item2 + (Radius / 2)))}");
            //Console.Out.WriteLine($"{RecursionDepth} : {Radius}  : {CenterPosition}  : {new Tuple<long, long>((CenterPosition.Item1 - (Radius / 2)), (CenterPosition.Item2 - (Radius / 2)))}");
            //Console.Out.WriteLine($"{RecursionDepth} : {Radius}  : {CenterPosition}  : {new Tuple<long, long>((CenterPosition.Item1 + (Radius / 2)), (CenterPosition.Item2 - (Radius / 2)))}");

            return true;
        }

        #endregion

        #region Search

        [CanBeNull]
        public TNodeData ClosestDataInsideTheNode(Tuple<long, long> position, out long distance,
            [CanBeNull] out QuadTreeContainerNode<TNodeData> containingNode)
        {
            var node = this;
            var dataList = new List<TNodeData>();

            while (node?.GetNode(position) != node)
            {
                node = node!.GetNode(position);
            }

            if (node!.Data == null)
            {
                node = node._parentNode ?? node;

                while (node._parentNode != null && GetDataNeighboringEmptyNode(node, position, dataList) == 0)
                {
                    node = node._parentNode;
                }

                if (!dataList.Any())
                {
                    distance = default;
                    containingNode = default;
                    return default;
                }
            }

            else
            {
                dataList.Add(node.Data);
            }


            var closestNode = dataList.FirstOrDefault();
            long closestNodeRadius = 0;

            if (closestNode != null)
                closestNodeRadius = MathOpt.GetDistance(position, closestNode.Position);

            foreach (var quadTreeKey in dataList)
            {
                var radius = MathOpt.GetDistance(position, quadTreeKey.Position);

                if (radius < closestNodeRadius)
                {
                    closestNodeRadius = radius;
                    closestNode = quadTreeKey;
                }
            }

            distance = closestNodeRadius;
            containingNode = node;
            return closestNode;
        }

        public void FindClosestNode(Tuple<long, long> position, [CanBeNull] ref TNodeData closestNode, long distance,
            [CanBeNull] QuadTreeContainerNode<TNodeData> containerNode)
        {
            if (containerNode == null || distance == 0) return;

            while ((distance > Math.Abs(position.Item2 - containerNode.CenterPosition.Item2) ||
                    distance > Math.Abs(position.Item1 - containerNode.CenterPosition.Item1)) &&
                   containerNode._parentNode != null)
            {
                var oldContainer = containerNode;
                containerNode = containerNode._parentNode;

                containerNode._upRightNode!.FindNodeRec(oldContainer, position, ref distance, ref closestNode);
                containerNode._upLeftNode!.FindNodeRec(oldContainer, position, ref distance, ref closestNode);
                containerNode._downRightNode!.FindNodeRec(oldContainer, position, ref distance, ref closestNode);
                containerNode._downLeftNode!.FindNodeRec(oldContainer, position, ref distance, ref closestNode);
            }
        }

        private void FindNodeRec(QuadTreeContainerNode<TNodeData> oldContainer, Tuple<long, long> position,
            ref long distance, [CanBeNull] ref TNodeData data)
        {
            if (oldContainer != this)
            {
                void FindNodeRecursive(QuadTreeContainerNode<TNodeData> node, Tuple<long, long> position,
                    ref long distance,
                    [CanBeNull] ref TNodeData data)
                {
                    if (MathOpt.BoundingBoxesIntersect(position, distance, node.CenterPosition, node.Radius))
                    {
                        if (node._upRightNode == null)
                        {
                            if (node!._upRightNode == null)
                            {
                                if (node.Data != null)
                                {
                                    if (MathOpt.GetDistance(node.Data.Position, position) < distance)
                                    {
                                        distance = MathOpt.GetDistance(node.Data.Position, position);
                                        data = node.Data;
                                    }
                                }
                            }

                            return;
                        }


                        FindNodeRecursive(node._upRightNode, position, ref distance, ref data);
                        FindNodeRecursive(node._upLeftNode!, position, ref distance, ref data);
                        FindNodeRecursive(node._downRightNode!, position, ref distance, ref data);
                        FindNodeRecursive(node._downLeftNode!, position, ref distance, ref data);
                    }
                }

                FindNodeRecursive(this, position, ref distance, ref data);

                return;
            }

        }

        private void FindNode(QuadTreeContainerNode<TNodeData> oldContainer, Tuple<long, long> position,
            ref long distance, [CanBeNull] ref TNodeData data)
        {
            if (oldContainer != this)
            {
                if (MathOpt.BoundingBoxesIntersect(position, distance, CenterPosition, Radius))
                {
                    var node = this;
                    var oldNode = this;
                    var nodeData = default(TNodeData);

                    do
                    {
                        if (node!._upRightNode == null)
                        {
                            if (node.Data != null)
                            {
                                if (MathOpt.GetDistance(node.Data.Position, position) < distance)
                                {
                                    distance = MathOpt.GetDistance(node.Data.Position, position);
                                    nodeData = node.Data;
                                }
                            }

                            if (node != this && node._parentNode != null)
                            {
                                oldNode = node;
                                node = node._parentNode;
                                continue;
                            }

                            break;
                        }

                        if (oldNode == node._parentNode || oldNode == node)
                        {
                            if (MathOpt.BoundingBoxesIntersect(position, distance, node._upRightNode.CenterPosition,
                                    node._upRightNode.Radius))
                            {
                                oldNode = node;
                                node = node._upRightNode;
                                continue;
                            }

                            if (MathOpt.BoundingBoxesIntersect(position, distance, node._upLeftNode!.CenterPosition,
                                    node._upLeftNode.Radius))
                            {
                                oldNode = node;
                                node = node._upLeftNode;
                                continue;
                            }

                            if (MathOpt.BoundingBoxesIntersect(position, distance, node._downRightNode!.CenterPosition,
                                    node._downRightNode.Radius))
                            {
                                oldNode = node;
                                node = node._downRightNode;
                                continue;
                            }

                            if (MathOpt.BoundingBoxesIntersect(position, distance, node._downLeftNode!.CenterPosition,
                                    node._downLeftNode.Radius))
                            {
                                oldNode = node;
                                node = node._downLeftNode;
                                continue;
                            }

                            oldNode = node;
                            node = node._parentNode;
                            continue;
                        }

                        if (oldNode == node._upRightNode)
                        {
                            oldNode = node;
                            node = node._upLeftNode;
                            continue;
                        }

                        if (oldNode == node._upLeftNode)
                        {
                            oldNode = node;
                            node = node._downRightNode;
                            continue;
                        }

                        if (oldNode == node._downRightNode)
                        {
                            oldNode = node;
                            node = node._downLeftNode;
                            continue;
                        }

                        if (oldNode == node._downLeftNode)
                        {
                            oldNode = node;
                            node = node._parentNode;
                            continue;
                        }


                        throw new Exception($"recursion tree could not handle the search: {node} {oldNode}");
                    } while (node != this);


                    data = nodeData ?? data;
                }
            }
        }

        public void NodesInsideBoundingBox(Tuple<long, long> position, long distance, List<TNodeData> dataList)
        {
            if (MathOpt.BoundingBoxesIntersect(CenterPosition, Radius, position, distance))
            {
                if (Data != null)
                {
                    if (MathOpt.BoundingBoxesIntersect(position, distance, Data.Position, 0))
                        dataList.Add(Data);
                }
                else
                {
                    _downLeftNode?.NodesInsideBoundingBox(position, distance, dataList);
                    _downRightNode?.NodesInsideBoundingBox(position, distance, dataList);
                    _upLeftNode?.NodesInsideBoundingBox(position, distance, dataList);
                    _upRightNode?.NodesInsideBoundingBox(position, distance, dataList);
                }
            }
        }

        private static int GetDataNeighboringEmptyNode(QuadTreeContainerNode<TNodeData> node,
            Tuple<long, long> position, ICollection<TNodeData> dataList)
        {
            if (node.Data != null)
            {
                dataList.Add(node.Data);
                return 1;
            }

            if (node.Radius > 0 && node._downLeftNode != null)
            {
#pragma warning disable CS8604
                return GetDataNeighboringEmptyNode(node._downLeftNode, position, dataList) +
                       GetDataNeighboringEmptyNode(node._upLeftNode, position, dataList) +
                       GetDataNeighboringEmptyNode(node._downRightNode, position, dataList) +
                       GetDataNeighboringEmptyNode(node._upRightNode, position, dataList);
#pragma warning restore CS8604
            }

            return 0;
        }

        public string DisplayNode()
        {
            var sb = new StringBuilder();

            var str = "";
            for (int i = 0; i < RecursionDepth; i++)
            {
                str += " ";
            }

            sb.Append($"{str}");

            sb.AppendLine($"center : {CenterPosition}, Data : {Data}");

            if (_upRightNode != null)
            {
                sb.Append($"{_upRightNode!.DisplayNode()}");
                sb.Append($"{_upLeftNode!.DisplayNode()}");
                sb.Append($"{_downLeftNode!.DisplayNode()}");
                sb.Append($"{_downRightNode!.DisplayNode()}");
            }

            return sb.ToString();
        }

        #endregion
    }
}