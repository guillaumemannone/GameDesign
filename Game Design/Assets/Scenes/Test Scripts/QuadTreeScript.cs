using System;
using Assets.Libs;
using Assets.Libs.Grid;
using Assets.Libs.Utils.QuadTree;
using UnityEngine;

namespace Assets.Scenes.Test_Scripts
{
    public class QuadTreeScript : QuadTreeGrid<BiodiversityChunk>
    {
        public GameObject DefaultTile;
        public long MaxPrey;
        public long MaxPredator;

        public void Start()
        {
            InstantiateNodes();
        }

        protected override void IncreaseTreeSize(Tuple<long, long> position)
        {
            MaxPrey *= 4;
            MaxPredator *= 4;
            var data = new DataNode()
            {
                Predator = MaxPredator,
                Prey = MaxPrey,
                DefaultTile = DefaultTile,
                RootNode = RootNode,
            };

            RootNode.IncreaseDepth(DefaultTile, data);
        }

        public override GameObject CreateRootChunk(Tuple<long, long> position)
        {
            var chunk = Instantiate(DefaultTile, transform);
            chunk.transform.position = new Vector3(MainCameraTransform.position.x, MainCameraTransform.position.y, 0);

            var component =  chunk.AddComponent<BiodiversityChunk>();

            component.Predator = MaxPredator;
            component.Prey = MaxPrey;
            component.Tile = DefaultTile;
            component.Radius = (int)(MinNodeRadius * Mathf.Pow(2, MaxRecursionDepth));
            component.Position = position;
            component.name = "init" + $"({position.Item1}, {position.Item2})";

            chunk.transform.localScale = new Vector3((float)component.Radius * 2, (float)component.Radius * 2);

            return chunk;
        }
    }
}
