using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Libs.Utils.QuadTree;
using UnityEngine;

namespace Assets.Libs.Grid
{
    public class BiodiversityChunk : MonoBehaviour, IQuadTreeObject
    {
        public double Prey;
        public double Predator;
        public GameObject Tile;
        public long Radius { get; set; }

        public BiodiversityChunk XChunk;
        public BiodiversityChunk YChunk;

        public Tuple<long, long> Position;

        private readonly List<double> _preyMigrationValues = new ();
        private readonly List<double> _predatorMigrationValues = new ();

        public void LoadValues()
        {
            PreyPredatorMigration(XChunk);
            PreyPredatorMigration(YChunk);
        }

        public void OnTimePassed()
        {
            double preyMigration = 0;
            double predatorMigration = 0;

            if (_preyMigrationValues.Any())
                preyMigration = _preyMigrationValues.Sum();
            _preyMigrationValues.Clear();

            if (_predatorMigrationValues.Any())
                predatorMigration = _predatorMigrationValues.Sum();
            _predatorMigrationValues.Clear();

            Prey += Prey * (0.002 - Predator * 0.005) + preyMigration;
            Predator += Predator * ( Prey * 0.001 - 0.002) + predatorMigration;

            Prey = (Prey < 0) ? 0 : Prey;
            Predator = (Predator < 0) ? 0 : Predator;

            Tile.GetComponent<SpriteRenderer>().color = Color.Lerp(Color.blue, Color.red, Mathf.Cos((float)((Prey - Predator) / (Predator + Prey + 1))));
        }

        private void PreyPredatorMigration(BiodiversityChunk chunk)
        {
            if (chunk != null)
            {
                var preyMigration = 0.001 * (Prey + chunk.Prey) * (chunk.Predator - Predator) / (Prey + chunk.Prey + 1);
                var predatorMigration = 0.001 * (Predator + chunk.Predator) * (chunk.Prey - Prey) / (Predator + chunk.Predator + 1);

                if (preyMigration < 0) preyMigration = (preyMigration < Prey) ? preyMigration : Prey;
                else preyMigration = (preyMigration < chunk.Prey) ? preyMigration : chunk.Prey;

                if (predatorMigration < 0) predatorMigration = (predatorMigration < Predator) ? predatorMigration : Predator;
                else predatorMigration = (predatorMigration < chunk.Predator) ? predatorMigration : chunk.Predator;


                _preyMigrationValues.Add(preyMigration);
                chunk._preyMigrationValues.Add(preyMigration);

                _predatorMigrationValues.Add(predatorMigration);
                chunk._predatorMigrationValues.Add(predatorMigration);
            }
        }


        public GameObject[] CutInFourChunks()
        {
            var octreeArray = new GameObject[4];
            for (int i = 0; i < 4; i++)
            {
                octreeArray[i] = CreateNewChunk(i);
            }

            return octreeArray;
        }

        public void InitializeNode(object nodeData)
        {
            var data = (DataNode)nodeData;


            Position = data.RootNode.CenterPosition;
            Radius = data.RootNode.Radius;
            Tile = data.DefaultTile;
            Prey = data.Prey;
            Predator = data.Predator;
            GetComponent<Transform>().localScale = new Vector3(Radius * 2, Radius * 2);
        }

        public void DestroyGameObject()
        {
            Destroy(gameObject);
        }

        public void DestroyChunk()
        {
            Destroy(GetComponent<SpriteRenderer>());
            Destroy(GetComponent<BiodiversityChunk>());
        }

        private GameObject CreateNewChunk(int index)
        {
            var newChunk = Instantiate(Tile, transform);
            newChunk.GetComponent<Transform>().localScale = new Vector3((float)0.5, (float)0.5);

            var component = newChunk.AddComponent<BiodiversityChunk>();

            component.Prey = Prey / 4;
            component.Predator = Predator / 4;
            component.Tile = Tile;
            component.Radius = Radius / 2;

            if (index == 0)
            {
                component.Position = new Tuple<long, long>((Position.Item1 - Radius / 2), (Position.Item2 + Radius / 2));
                newChunk.transform.position = new Vector3(
                    (Position.Item1 - Radius / 2),
                    (Position.Item2 + Radius / 2), 0);
            }
            else if (index == 1)
            {
                component.Position = new Tuple<long, long>((Position.Item1 + Radius / 2), (Position.Item2 + Radius / 2));
                newChunk.transform.position = new Vector3(
                    (Position.Item1 + Radius / 2),
                    (Position.Item2 + Radius / 2), 0);
            }
            else if (index == 2)
            {
                component.Position = new Tuple<long, long>((Position.Item1 - Radius / 2), (Position.Item2 - Radius / 2));
                newChunk.transform.position = new Vector3(
                    (Position.Item1 - Radius / 2),
                    (Position.Item2 - Radius / 2), 0);
            }
            else
            {
                component.Position = new Tuple<long, long>((Position.Item1 + Radius / 2), (Position.Item2 - Radius / 2));
                newChunk.transform.position = new Vector3(
                    (Position.Item1 + Radius / 2),
                    (Position.Item2 - Radius / 2), 0);
            }

            return newChunk;
        }
    }
}
