﻿using System;
using UnityEngine;

namespace Assets.Libs.Skills.Essence.Implementation
{
    [Serializable]
    public class AuraEssence : IEssence
    {
        public EssenceType Type => EssenceType.Aura;

        public int EssenceTier { get; }
        
        public float EssenceContained { get; }

        public AuraEssence(int essenceTier, float essenceContained)
        {
            EssenceTier = essenceTier;
            EssenceContained = essenceContained;
        }
    }
}