using System;
using UnityEngine;

namespace Assets.Libs.Grid
{
    public class BiodiversityGrid : ProceduralGridGeneration<GameObject>
    {
        public GameObject DefaultTile;

        public override GameObject InstantiateChunk(Tuple<int, int> key)
        {
            var chunk = Instantiate(DefaultTile, transform);
            var biodivChunk = chunk.AddComponent<BiodiversityChunk>();

            biodivChunk.Prey = 20;
            biodivChunk.Predator = 0.1;
            biodivChunk.Tile = chunk;

            if (Grid.TryGetValue(new Tuple<int, int>(key.Item1 + 1, key.Item2), out var xChunk))
                biodivChunk.XChunk = xChunk.GetComponent<BiodiversityChunk>();

            if (Grid.TryGetValue(new Tuple<int, int>(key.Item1 - 1, key.Item2), out var minusXChunk))
                minusXChunk.GetComponent<BiodiversityChunk>().XChunk = biodivChunk;

            if (Grid.TryGetValue(new Tuple<int, int>(key.Item1, key.Item2 + 1), out var yChunk))
                biodivChunk.YChunk = yChunk.GetComponent<BiodiversityChunk>();

            if (Grid.TryGetValue(new Tuple<int, int>(key.Item1, key.Item2 - 1), out var minusYChunk))
                minusYChunk.GetComponent<BiodiversityChunk>().YChunk = biodivChunk;

            chunk.name = $"chunk {key.Item1}, {key.Item2}";
            chunk.transform.localPosition = new Vector3(key.Item1, key.Item2, 0);

            return chunk;
        }

        public override void DestroyChunk(Tuple<int, int> key)
        {
            Grid.TryGetValue(key, out var chunk);

            if (Grid.TryGetValue(new Tuple<int, int>(key.Item1 - 1, key.Item2), out var minusXChunk))
                minusXChunk.GetComponent<BiodiversityChunk>().XChunk = null;

            if (Grid.TryGetValue(new Tuple<int, int>(key.Item1, key.Item2 - 1), out var minusYChunk))
                minusYChunk.GetComponent<BiodiversityChunk>().XChunk = null;

            Destroy(chunk);
        }

        public override void UpdateChunks()
        {
            foreach (var keyValuePair in Grid)
            {
                keyValuePair.Value.GetComponent<BiodiversityChunk>().LoadValues();
            }

            foreach (var keyValuePair in Grid)
            {
                keyValuePair.Value.GetComponent<BiodiversityChunk>().OnTimePassed();
            }
        }
    }
}
