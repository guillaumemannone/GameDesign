using Assets.Libs.Items;
using NUnit.Framework;
using UnityEngine;

namespace Assets.Tests
{
    public class TestsItems
    {
        class TestItem : IBaseItem
        {
            public string Name { get; }
            public float Weight { get; }
            public float Volume { get; }
            public ItemType Type { get; }

            public TestItem(string name, float weight, float volume)
            {
                Name = name;
                Weight = weight;
                Volume = volume;
                Type = ItemType.Material;
            }


            public void Drop(GameObject tile)
            {
                throw new System.NotImplementedException();
            }

            public void Throw()
            {
                throw new System.NotImplementedException();
            }
        }


        [Test]
        public void TestsStoreAnItem()
        {
            var inventory = new Inventory(0.5f);

            var item = new TestItem("test item", 2f, 0.1f);

            Assert.IsTrue(inventory.StoreItem(item));
        }


        [Test]
        public void TestsRemoveAnItem()
        {
            var inventory = new Inventory(0.5f);

            var item = new TestItem("test item", 2f, 0.1f);

            Assert.IsTrue(inventory.StoreItem(item));
            Assert.IsTrue(inventory.RemoveItem(item));
        }


        [Test]
        public void TestsAddingAnItemTwiceDoesNothing()
        {
            var inventory = new Inventory(0.5f);

            var item = new TestItem("test item", 2f, 0.1f);

            Assert.IsTrue(inventory.StoreItem(item));
            Assert.IsFalse(inventory.StoreItem(item));
        }

        [Test]
        public void TestsSaveAndLoadItem()
        {
            var item = new TestItem("test item", 2f, 0.1f);



        }

        [Test]
        public void TestsSaveAndLoadInventory()
        {

        }
    }
}
