using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Libs.Grid
{
    public abstract class ProceduralGridGeneration<TChunk> : MonoBehaviour
    {
        protected Dictionary<Tuple<int, int>, TChunk> Grid = new();

        protected Tuple<int, int> Position = new(0,0);

        public int Radius;
        public int GridSize;


        public void InstantiateGrid()
        {
            for (int i = -Radius; i < Radius; i++)
            {
                for (int j = -Radius; j < Radius; j++)
                {
                    var key = new Tuple<int, int>(i, j);
                    Grid.Add(key, InstantiateChunk(key));
                }
            }
        }

        public void OnMovement(Tuple<int, int> position)
        {
            var newPosition = new Tuple<int, int>(position.Item1 / GridSize, position.Item2 / GridSize);

            if (newPosition.Equals(Position)) return;

            var xSurplus = newPosition.Item1 - Position.Item1;
            var ySurplus = newPosition.Item2 - Position.Item2;

            Position = newPosition;

            if (xSurplus > 2 * Radius || ySurplus > 2 * Radius)
            {
                foreach (var keyValuePair in Grid)
                {
                    DestroyChunk(keyValuePair.Key);
                }

                Grid.Clear();

                InstantiateGrid();
            }
            else
            {
                var xSign = xSurplus.CompareTo(0);
                var ySign = ySurplus.CompareTo(0);

                InstantiateChunkOnMovement(xSign, Position.Item1, xSurplus, Position.Item2, false);
                InstantiateChunkOnMovement(ySign, Position.Item2, ySurplus, Position.Item1, true);
            }
        }

        private void InstantiateChunkOnMovement(int sign, int position, int surplus, int otherPosition, bool inverse)
        {
            int maxValue;
            int minValue;
            Tuple<int, int> pos;

            if (position - (sign * Radius) < position - (sign * Radius + surplus))
            {
                maxValue = position - (sign * Radius + surplus);
                minValue = position - (sign * Radius);
            }
            else
            {
                minValue = position - (sign * Radius + surplus);
                maxValue = position - (sign * Radius);
            }

            for (int i = minValue; i < maxValue; i++)
            {
                for (int j = otherPosition - Radius - 1; j < otherPosition + Radius + 1; j++)
                {
                    pos = inverse ? new Tuple<int, int>(j, i) : new Tuple<int, int>(i, j);

                    if (Grid.TryGetValue(pos, out _))
                    {
                        DestroyChunk(pos);
                        Grid.Remove(pos); 
                    }
                }
            }

            if (position + (sign * Radius) < position + (sign * Radius - surplus))
            {
                maxValue = position + (sign * Radius - surplus);
                minValue = position + (sign * Radius);
            }
            else
            {
                maxValue = position + (sign * Radius);
                minValue = position + (sign * Radius - surplus);
            }


            for (int i = minValue; i < maxValue; i++)
            {
                for (int j = otherPosition - Radius; j < otherPosition + Radius; j++)
                {
                    pos = inverse ? new Tuple<int, int>(j, i) : new Tuple<int, int>(i, j);
                    if (!Grid.TryGetValue(pos, out _))
                        Grid.TryAdd(pos, InstantiateChunk(pos));
                }
            }
        }


        public abstract TChunk InstantiateChunk(Tuple<int, int> key);

        public abstract void DestroyChunk(Tuple<int, int> key);

        public abstract void UpdateChunks();
    }
}
