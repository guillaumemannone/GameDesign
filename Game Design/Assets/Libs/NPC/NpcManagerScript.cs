using System.Collections.Generic;
using System.Linq;
using Assets.Libs.Utils.QuadTree;
using Unity.VisualScripting.YamlDotNet.Core.Tokens;
using UnityEngine;

namespace Assets.Libs.NPC
{
    public class NpcManagerScript : MonoBehaviour
    {
        public QuadTreeContainer<INpcBehavior> NpcQuadTree = new();
        public SortedList<double, INpcBehavior> NpcQueue = new();


        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                var Npc = NpcQueue.FirstOrDefault();
                NpcQueue.RemoveAt(0);

                void PlayNpcTurns()
                {
                    do
                    {
                        NpcQueue.Add(Npc.Key + Npc.Value.PlayTurn(), Npc.Value);

                        Npc = NpcQueue.FirstOrDefault();
                        NpcQueue.RemoveAt(0);

                    } while (Npc.Value is { IsPlayer: false });
                }

                if (Npc.Value.IsPlayer)
                {
                    PlayNpcTurns();
                }
                else
                {
                    PlayNpcTurns();
                    PlayNpcTurns();
                }
            }
        }

        public void LoadGame(SortedList<double, INpcBehavior> npcQueue)
        {
            NpcQueue = npcQueue;
            NpcQuadTree = new QuadTreeContainer<INpcBehavior>();

            foreach (var keyValuePair in NpcQueue)
            {
                NpcQuadTree.TryAdd(keyValuePair.Value);
            }
        }
    }
}
