﻿using System;
using Assets.Libs.Skills.Essence.Implementation;

namespace Assets.Libs.Skills.Essence
{
    [Serializable]
    public class EssenceContainer
    {
        public IEssence AuraEssence { get; }

        public IEssence KiEssence { get; }

        public  IEssence ManaEssence { get; }

        public EssenceContainer()
        {
            AuraEssence = new AuraEssence(0, 0);
            KiEssence = new KiEssence(0, 0);
            ManaEssence = new ManaEssence(0, 0);
        }

        public EssenceContainer(IEssence auraEssence, IEssence kiEssence, IEssence manaEssence)
        {
            AuraEssence = auraEssence;
            KiEssence = kiEssence;
            ManaEssence = manaEssence;
        }
    }
}