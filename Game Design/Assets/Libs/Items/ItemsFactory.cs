﻿using System;

namespace Assets.Libs.Items
{
    public class ItemsFactory
    {
        public int Seed { get; set; }

        public IBaseItem CreateItem(ItemType itemType)
        {
            switch (itemType)
            {
                case ItemType.Material:
                    CreateMaterial();
                    break;
                case ItemType.Armor:
                    CreateArmor();
                    break;
                case ItemType.Food:
                    CreateFood();
                    break;
                case ItemType.Potion:
                    CreatePotion();
                    break;
                case ItemType.Weapon:
                    CreateWeapon();
                    break;
                default:
                    throw new Exception($"Item Type {itemType} not implemented");
            }

            return default;
        }

        private void CreateArmor()
        {

        }

        private void CreateMaterial()
        {

        }

        private void CreateFood()
        {

        }

        private void CreateWeapon()
        {

        }

        private void CreatePotion()
        {

        }

        private void CreateElixir()
        {

        }
    }
}