using System;
using Assets.Libs.Grid;
using UnityEngine;

namespace Assets.Scenes.Test_Scripts
{
    public class CameraScript : MonoBehaviour
    {
        // Update is called once per frame
        public BiodiversityGrid Grid;
        public QuadTreeScript Oct;

        void Update()
        {
            if (Input.anyKey)
            {
                Camera.main.transform.Translate(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"), 0);
                var vecPosition = Camera.main.transform.localPosition;

                var position = new Tuple<int, int>((int)vecPosition.x, (int)vecPosition.y);
                Grid.OnMovement(position);

                var otherposition = new Tuple<long, long>((long)vecPosition.x, (long)vecPosition.y);
                Oct.OnMovement(otherposition);
            }
        }
    }
}
