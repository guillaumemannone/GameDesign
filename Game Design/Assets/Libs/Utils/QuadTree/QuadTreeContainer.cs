﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace Assets.Libs.Utils.QuadTree
{
    public class QuadTreeContainer<TData> where TData : IQuadTreeKey
    {

        private QuadTreeContainerNode<TData> _rootNode;

        public QuadTreeContainer()
        {
            _rootNode = QuadTreeContainerNode<TData>.CreateRootNode();
        }


        public bool? TryAdd(TData data)
        {
            while (_rootNode.Radius <= Math.Abs(data.Position.Item2) || _rootNode.Radius <= Math.Abs(data.Position.Item1))
            {
                _rootNode = _rootNode.IncreaseRootDepth();
            }

            return _rootNode.StoreData(data);
        }

        public bool TryRemove(Tuple<long, long> position, [CanBeNull] out TData removedData)
        {
            removedData = _rootNode.RemoveDataAtPosition(position);
            return removedData != null;
        }

        [CanBeNull]
        public TData GetClosestDataFromPosition(Tuple<long, long> position)
        {
            var closestNode = _rootNode.ClosestDataInsideTheNode(position, out var distance, out var containingNode);

            _rootNode.FindClosestNode(position, ref closestNode, distance, containingNode);

            return closestNode;
        }

        public IEnumerable<TData> DataInRegion(Tuple<long, long> position, long radius)
        {
            var dataList = new List<TData>();

            _rootNode.NodesInsideBoundingBox(position, radius, dataList);

            return dataList;
        }

        [CanBeNull]
        public TData NearestNeighbor(TData data)
        {
            return GetClosestDataFromPosition(data.Position);
        }
    }

    public interface IQuadTreeKey
    {
        Tuple<long, long> Position { get; set; }
    }
}