﻿using System;
using UnityEngine;

namespace Assets.Scenes.Arena_Scripts
{
    public class CameraScripts : MonoBehaviour
    {
        void Update()
        {
            if (Input.anyKeyDown)
            {
                Camera.main.transform.Translate(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"), 0);
            }
        }
    }
}