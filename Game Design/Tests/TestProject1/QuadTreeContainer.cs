﻿namespace TestQuadTree
{
    public class QuadTreeContainer<TData> where TData : IQuadTreeKey
    {

        private QuadTreeContainerNode<TData> RootNode;

        public QuadTreeContainer()
        {
            RootNode = QuadTreeContainerNode<TData>.CreateRootNode();
        }


        public bool? TryAdd(TData data)
        {
            while (RootNode.Radius <= Math.Abs(data.Position.Item2) || RootNode.Radius <= Math.Abs(data.Position.Item1))
            {
                RootNode = RootNode.IncreaseRootDepth();
            }

            return RootNode.StoreData(data);
        }

        public bool TryRemove(Tuple<long, long> position, out TData? removedData)
        {
            removedData = RootNode.RemoveDataAtPosition(position);
            return removedData != null;
        }

        public TData? GetClosestDataFromPosition(Tuple<long, long> position)
        {
            var closestNode = RootNode.ClosestDataInsideTheNode(position, out var distance, out var containingNode);

            RootNode.FindClosestNode(position, ref closestNode, distance, containingNode);

            return closestNode;
        }

        public IEnumerable<TData> DataInRegion(Tuple<long, long> position, long radius)
        {
            var dataList = new List<TData>();

            RootNode.NodesInsideBoundingBox(position, radius, dataList);

            return dataList;
        }

        public TData? NearestNeighbor(TData data)
        {
            return GetClosestDataFromPosition(data.Position);
        }
    }

    public interface IQuadTreeKey
    {
        Tuple<long, long> Position { get; set; }
    }
}