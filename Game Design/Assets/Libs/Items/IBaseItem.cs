﻿using UnityEngine;

namespace Assets.Libs.Items
{
    public interface IBaseItem
    {
        string Name { get; }
        float Weight { get; }
        float Volume { get; }
        ItemType Type { get; }


        void Drop(GameObject tile);
        void Throw();
    }
}