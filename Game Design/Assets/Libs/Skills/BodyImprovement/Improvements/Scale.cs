﻿namespace Assets.Libs.Skills.BodyImprovement.Improvements
{
    public class Scale : IImprovement
    {
        public ImprovementType ImprovementType => ImprovementType.Scale;
    }
}