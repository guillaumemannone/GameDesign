﻿namespace Assets.Libs.Items.Armor
{
    public enum HumanoidArmorType 
    {
        Helm,
        ChestArmor,
        Trousers,
        BackPack,
        LeftBracer,
        RightBracer,
        RightLeggings,
        LeftLeggings
    }
}