﻿namespace Assets.Libs.Skills.BodyImprovement.Improvements
{
    public enum ImprovementType
    {
        MagusCore,
        ManaBrain,
        Scale,
        Dantian,
        Meridian
    }
}