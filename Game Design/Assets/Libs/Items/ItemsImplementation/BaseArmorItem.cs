﻿using System;
using UnityEngine;

namespace Assets.Libs.Items.ItemsImplementation
{
    [Serializable]
    public class BaseArmorItem : IBaseItem
    {
        public string Name { get; }
        public float Weight { get; }
        public float Volume { get; }
        public int ArmorKind { get; }
        public int ArmorType { get; }

        public ItemType Type { get; }

        public float ArmorValue { get; }


        public BaseArmorItem(string name, float weight, float volume, int armorKind, int armorType, float armorValue)
        {
            Name = name;
            Weight = weight;
            Volume = volume;
            ArmorKind = armorKind;
            ArmorType = armorType;
            ArmorValue = armorValue;
        }


        public void Store(GameObject npc)
        {
            throw new System.NotImplementedException();
        }

        public void Drop(GameObject tile)
        {
            throw new System.NotImplementedException();
        }

        public void Throw()
        {
            throw new System.NotImplementedException();
        }
    }
}