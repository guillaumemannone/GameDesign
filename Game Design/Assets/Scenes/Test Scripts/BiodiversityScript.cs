using System;
using System.Collections.Generic;
using Assets.Libs.Grid;
using TMPro;
using UnityEngine;

namespace Assets.Scenes.Scripts
{
    public class BiodiversityScript : MonoBehaviour
    {
        public TextMeshProUGUI TimeDisplay;
        public BiodiversityGrid BiodiversityGrid;
        
        private int _time;


        void Start()
        {
            TimeDisplay.text = $"Time: {_time}";
            BiodiversityGrid.InstantiateGrid();
        }


        void Update()
        {
            if (Input.anyKey)
            {
                _time++;
                TimeDisplay.text = $"Time: {_time}";
                BiodiversityGrid.UpdateChunks();
            }
        }
    }
}
