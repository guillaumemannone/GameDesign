using System;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.TextCore.Text;

namespace Assets.Scenes.Arena_Scripts
{
    public class ArenaTerrainScript : MonoBehaviour
    {
        public GameObject DefaultTile;
        public GameObject Character;
        public int Radius;
        public Camera Cam;

        protected Dictionary<Tuple<long, long>, GameObject> Grid = new();

        private GameObject _character;

        // Start is called before the first frame update
        void Start()
        {
            InstantiateGrid();
            AddCharacter();
        }

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                OnClick(Cam.ScreenToWorldPoint(Input.mousePosition, Cam.stereoActiveEye));
            }

            if (Input.GetKeyDown(KeyCode.Space))
            {
                PassTurn();
            }
        }

        public void OnClick(Vector3 position)
        {
            var positionTuple = new Tuple<long, long>((long)math.ceil(position.x), (long)math.ceil(position.y));
            if (Grid.TryGetValue(positionTuple, out var tile))
            {
                tile.GetComponent<SpriteRenderer>().color = Color.blue;
                _character.transform.localPosition = tile.transform.localPosition + Vector3.back;
            }
        }

        private void InstantiateGrid()
        {
            for (long i = -Radius; i < Radius; i++)
            {
                for (long j = -Radius; j < Radius; j++)
                {
                    var key = new Tuple<long, long>(i, j);
                    var tile = Instantiate(DefaultTile, transform);
                    Grid.Add(key, tile);
                    tile.transform.localPosition = new Vector3(key.Item1 - 0.5f, key.Item2 - 0.5f, 0);
                }
            }
        }

        private void AddCharacter()
        {
            var character = Instantiate(Character, transform);
            character.transform.localPosition = new Vector3(0.5f, 0.5f, -1);
            character.transform.localScale = new Vector3(0.2f, 0.2f, 1);
            _character = character;
        }

        private void PassTurn()
        {

        }
    }
}
