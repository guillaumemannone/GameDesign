﻿using System;
using UnityEngine;

namespace Assets.Libs.NPC
{
    [Serializable]
    public class PlayerScript : MonoBehaviour, INpcBehavior
    {

        public NpcData Data = new();


        public Tuple<long, long> Position { get; set; }
        public Tuple<long, long> Destination { get; set; }


        public bool IsPlayer => true;


        public double PlayTurn()
        {


            return 0;
        }

        public void OnDeath()
        {

        }
    }
}