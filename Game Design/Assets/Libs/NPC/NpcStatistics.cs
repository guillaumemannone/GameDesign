﻿using System;
using System.Collections.Generic;
using Assets.Libs.Items;
using Assets.Libs.Items.ItemsImplementation;
using Assets.Libs.Skills.BodyImprovement;

namespace Assets.Libs.NPC
{
    [Serializable]
    public class NpcData
    {
        public Dictionary<int, BaseArmorItem> Armors { get; set; }
        public Inventory Inventory { get; set; }
        public BodyState Improvements { get; }


        public int Health { get; set; }
        public int MaxHealth { get; set; }
        public int Mana { get; set; }
        public int MaxMana { get; set; }

        public int Speed { get; set; }
        public int Endurance { get; set; }
        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }

        public NpcData()
        {
            Armors = new Dictionary<int, BaseArmorItem>();
            Inventory = new Inventory(0f);
            Improvements = new BodyState();

            Health = 100;
            MaxHealth = 100;

            Mana = 100;
            MaxMana = 100;

            Speed = 100;
            Endurance = 100;
            Strength = 100;
            Dexterity = 100;
            Intelligence = 100;
        }

        public NpcData(
            Dictionary<int, BaseArmorItem> armors, 
            Inventory inventory, 
            BodyState improvements, 
            
            int health, int maxHealth, int mana, int maxMana, int speed, 
            int endurance, int strength, int dexterity, int intelligence)
        {
            Armors = armors;
            Inventory = inventory;
            Improvements = improvements;
            Health = health;
            MaxHealth = maxHealth;
            Mana = mana;
            MaxMana = maxMana;
            Speed = speed;
            Endurance = endurance;
            Strength = strength;
            Dexterity = dexterity;
            Intelligence = intelligence;
        }
    }
}