﻿using Assets.Libs.Skills.BodyImprovement.Improvements;

namespace Assets.Libs.Skills.BodyImprovement
{
    public interface IImprovement
    {
        public ImprovementType ImprovementType { get; }


    }
}