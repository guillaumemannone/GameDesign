﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using Assets.Libs;
using UnityEngine;

namespace Assets
{
    public static class GameDataManager
    {

        private static BinaryFormatter _binaryFormatter = new();

        public static void LoadChunk()
        {

        }

        public static void SaveChunk()
        {

        }

        public static GameInstanceData LoadGame(string saveName)
        {
            if (File.Exists(saveName))
            {
                var file = File.Open(Path.Combine(Application.persistentDataPath + saveName), FileMode.Open);

                GameInstanceData data = (GameInstanceData) _binaryFormatter.Deserialize(file);
                file.Close();

                return data;
            }

            return new GameInstanceData();
        }

        public static void SaveGame(GameInstanceData data, string saveName)
        {
            var file = File.Create(Path.Combine(Application.persistentDataPath + saveName));

            _binaryFormatter.Serialize(file, data);

            file.Close();
        }
    }
}