﻿namespace Assets.Libs.Items
{
    public enum ItemType
    {
        Armor,
        Material,
        Potion,
        Elixir,
        Weapon,
        Food,
    }
}