﻿using System;
using System.Collections.Generic;
using Assets.Libs.Skills.BodyImprovement.Improvements;
using Assets.Libs.Skills.Essence;

namespace Assets.Libs.Skills.BodyImprovement
{
    public class BodyState
    {
        public EssenceContainer BodyEssence { get; }

        public Dictionary<ImprovementType, IImprovement> Improvements { get; }


        public BodyState()
        {
            BodyEssence = new EssenceContainer();
            Improvements = new Dictionary<ImprovementType, IImprovement>();
        }

        public BodyState(EssenceContainer bodyEssence, Dictionary<ImprovementType, IImprovement> improvements)
        {
            BodyEssence = bodyEssence;
            Improvements = improvements;
        }

        public bool AddImprovement(IImprovement improvement)
        {
            if (Improvements.ContainsKey(improvement.ImprovementType)) return false;

            if (!Improvements.TryAdd(improvement.ImprovementType, improvement))
            {
                throw new Exception($"could not add improvement");
            }

            return true;
        }

        public bool RemoveImprovement(ImprovementType improvementType)
        {
            if (!Improvements.ContainsKey(improvementType)) return false;

            if (!Improvements.Remove(improvementType))
            {
                throw new Exception($"could not remove improvement");
            }

            return true;
        }
    }
}