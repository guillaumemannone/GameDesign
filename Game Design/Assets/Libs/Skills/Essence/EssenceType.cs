﻿using log4net.Appender;

namespace Assets.Libs.Skills.Essence
{
    public enum EssenceType
    {
        Mana,
        Aura,
        Ki
    }
}