using System;
using System.Collections;
using Assets.Libs.Utils.QuadTree;
using NUnit.Framework;
using UnityEngine.TestTools;

namespace Assets.Tests
{

    public class NodeDataTest : IQuadTreeKey
    {
        public NodeDataTest(long x, long y)
        {
            Position = new Tuple<long, long>(x, y);
        }

        public Tuple<long, long> Position { get; set; }
    }

    public class TestsUtils
    {
        private QuadTreeContainer<NodeDataTest> _container = new();

        [SetUp]
        public void SetUp()
        {
            _container = new QuadTreeContainer<NodeDataTest>();
        }



        [Test]
        public void TestAddAndRemoveNodes()
        {
            Assert.IsTrue(_container.TryAdd(new NodeDataTest(0, 0)));
            Assert.IsFalse(_container.TryAdd(new NodeDataTest(0, 0)));

            Assert.IsTrue(_container.TryRemove(new Tuple<long, long>(0, 0), out var data));
            Assert.IsNotNull(data);
            Assert.IsFalse(_container.TryRemove(new Tuple<long, long>(0, 0), out data));
            Assert.IsNull(data);
        }



        [Test]
        public void TestAddAndRemoveMultipleNodesOnXAxis()
        {
            Assert.IsTrue(_container.TryAdd(new NodeDataTest(0, 0)));
            Assert.IsTrue(_container.TryAdd(new NodeDataTest(0, 1)));
            Assert.IsTrue(_container.TryAdd(new NodeDataTest(0, 3)));
            Assert.IsTrue(_container.TryAdd(new NodeDataTest(0, 9)));
            Assert.IsTrue(_container.TryAdd(new NodeDataTest(0, 17)));

            Assert.IsTrue(_container.TryRemove(new Tuple<long, long>(0, 0), out var data));
            Assert.AreEqual(data.Position.Item2, 0);

            Assert.IsTrue(_container.TryRemove(new Tuple<long, long>(0, 1), out data));
            Assert.AreEqual(data.Position.Item2, 1);

            Assert.IsTrue(_container.TryRemove(new Tuple<long, long>(0, 3), out data));
            Assert.AreEqual(data.Position.Item2, 3);

            Assert.IsTrue(_container.TryRemove(new Tuple<long, long>(0, 9), out data));
            Assert.AreEqual(data.Position.Item2, 9);

            Assert.IsTrue(_container.TryRemove(new Tuple<long, long>(0, 17), out data));
            Assert.AreEqual(data.Position.Item2, 17);


            Assert.IsTrue(_container.TryAdd(new NodeDataTest(0, 0)));
            Assert.IsTrue(_container.TryAdd(new NodeDataTest(0, -1)));
            Assert.IsTrue(_container.TryAdd(new NodeDataTest(0, -3)));
            Assert.IsTrue(_container.TryAdd(new NodeDataTest(0, -9)));
            Assert.IsTrue(_container.TryAdd(new NodeDataTest(0, -17)));

            Assert.IsTrue(_container.TryRemove(new Tuple<long, long>(0, 0), out data));
            Assert.AreEqual(data.Position.Item2, -0);

            Assert.IsTrue(_container.TryRemove(new Tuple<long, long>(0, -1), out data));
            Assert.AreEqual(data.Position.Item2, -1);

            Assert.IsTrue(_container.TryRemove(new Tuple<long, long>(0, -3), out data));
            Assert.AreEqual(data.Position.Item2, -3);

            Assert.IsTrue(_container.TryRemove(new Tuple<long, long>(0, -9), out data));
            Assert.AreEqual(data.Position.Item2, -9);

            Assert.IsTrue(_container.TryRemove(new Tuple<long, long>(0, -17), out data));
            Assert.AreEqual(data.Position.Item2, -17);
        }



        [Test]
        public void TestAddAndRemoveMultipleNodesOnYAxis()
        {
            Assert.IsTrue(_container.TryAdd(new NodeDataTest(0, 0)));
            Assert.IsTrue(_container.TryAdd(new NodeDataTest(1, 0)));
            Assert.IsTrue(_container.TryAdd(new NodeDataTest(3, 0)));
            Assert.IsTrue(_container.TryAdd(new NodeDataTest(9, 0)));
            Assert.IsTrue(_container.TryAdd(new NodeDataTest(17, 0)));

            Assert.IsTrue(_container.TryRemove(new Tuple<long, long>(0, 0), out var data));
            Assert.AreEqual(data.Position.Item1, 0);

            Assert.IsTrue(_container.TryRemove(new Tuple<long, long>(1, 0), out data));
            Assert.AreEqual(data.Position.Item1, 1);

            Assert.IsTrue(_container.TryRemove(new Tuple<long, long>(3, 0), out data));
            Assert.AreEqual(data.Position.Item1, 3);

            Assert.IsTrue(_container.TryRemove(new Tuple<long, long>(9, 0), out data));
            Assert.AreEqual(data.Position.Item1, 9);

            Assert.IsTrue(_container.TryRemove(new Tuple<long, long>(17, 0), out data));
            Assert.AreEqual(data.Position.Item1, 17);
        }



        [Test]
        public void TestAddAndRemoveMultipleNodesOnBothAxis()
        {
            Assert.IsTrue(_container.TryAdd(new NodeDataTest(0, 0)));
            Assert.IsTrue(_container.TryAdd(new NodeDataTest(1, 1)));
            Assert.IsTrue(_container.TryAdd(new NodeDataTest(3, 3)));
            Assert.IsTrue(_container.TryAdd(new NodeDataTest(9, 9)));
            Assert.IsTrue(_container.TryAdd(new NodeDataTest(17, 17)));
            Assert.IsTrue(_container.TryAdd(new NodeDataTest(17, 18)));

            Assert.IsTrue(_container.TryRemove(new Tuple<long, long>(0, 0), out var data));
            Assert.AreEqual(data.Position.Item2, 0);

            Assert.IsTrue(_container.TryRemove(new Tuple<long, long>(1, 1), out data));
            Assert.AreEqual(data.Position.Item2, 1);
            Assert.AreEqual(data.Position.Item1, 1);

            Assert.IsTrue(_container.TryRemove(new Tuple<long, long>(3, 3), out data));
            Assert.AreEqual(data.Position.Item2, 3);
            Assert.AreEqual(data.Position.Item1, 3);

            Assert.IsTrue(_container.TryRemove(new Tuple<long, long>(9, 9), out data));
            Assert.AreEqual(data.Position.Item2, 9);
            Assert.AreEqual(data.Position.Item1, 9);

            Assert.IsTrue(_container.TryRemove(new Tuple<long, long>(17, 17), out data));
            Assert.AreEqual(data.Position.Item2, 17);
            Assert.AreEqual(data.Position.Item1, 17);

            Assert.IsTrue(_container.TryRemove(new Tuple<long, long>(17, 18), out data));
            Assert.AreEqual(data.Position.Item2, 18);
            Assert.AreEqual(data.Position.Item1, 17);
        }

        [Test]
        public void TestSearchEmpty()
        {
            Assert.IsNull(_container.GetClosestDataFromPosition(position: new Tuple<long, long>(0, 0)));
        }

        [Test]
        public void TestSearchNotEmpty()
        {
            _container.TryAdd(new NodeDataTest(0, 0));

            var finding = _container.GetClosestDataFromPosition(position: new Tuple<long, long>(0, 0));
            Assert.IsNotNull(finding);


            finding = _container.GetClosestDataFromPosition(position: new Tuple<long, long>(0, 2));
            Assert.IsNotNull(finding);


            finding = _container.GetClosestDataFromPosition(position: new Tuple<long, long>(0, 10));
            Assert.IsNotNull(finding);
        }

        [Test]
        public void TestSearchWithMultipleNodes()
        {
            _container.TryAdd(new NodeDataTest(0, 0));
            _container.TryAdd(new NodeDataTest(0, 5));

            var finding = _container.GetClosestDataFromPosition(position: new Tuple<long, long>(0, 0));
            Assert.IsNotNull(finding);


            finding = _container.GetClosestDataFromPosition(position: new Tuple<long, long>(0, 2));
            Assert.IsNotNull(finding);
            Console.Out.WriteLine($"{finding?.Position}");


            finding = _container.GetClosestDataFromPosition(position: new Tuple<long, long>(0, 10));
            Assert.IsNotNull(finding);
            Console.Out.WriteLine($"{finding?.Position}");
        }


        [Test]
        public void Test()
        {
            _container.TryAdd(new NodeDataTest(0, 0));
            _container.TryAdd(new NodeDataTest(0, 5));
            _container.TryAdd(new NodeDataTest(-1, 10));


            var finding = _container.GetClosestDataFromPosition(position: new Tuple<long, long>(0, 0));
            Assert.IsNotNull(finding);


            finding = _container.GetClosestDataFromPosition(position: new Tuple<long, long>(0, 2));
            Assert.IsNotNull(finding);


            finding = _container.GetClosestDataFromPosition(position: new Tuple<long, long>(0, 10));
            Assert.IsNotNull(finding);
            Console.Out.WriteLine($"position {finding!.Position}");
        }


        [Test]
        public void TestBenchmarking()
        {
            var maxNumber = 100;


            for (var i = -maxNumber; i < maxNumber; i++)
            {
                for (var j = -maxNumber; j < maxNumber; j++)
                {
                    _container.TryAdd(new NodeDataTest(i * 10, j * 5));
                }
            }


            var finding = _container.GetClosestDataFromPosition(position: new Tuple<long, long>(0, 0));


            var numberFound = _container.DataInRegion(new Tuple<long, long>(0, 0), maxNumber * 10);


            for (var i = -maxNumber; i < maxNumber; i++)
            {
                for (var j = -maxNumber; j < maxNumber; j++)
                {
                    _container.DataInRegion(new Tuple<long, long>(i * 10, j * 5), 100);
                }
            }
        }
    }
}
