﻿using System;

namespace Assets.Libs.Skills.Essence.Implementation
{
    [Serializable]
    public class ManaEssence : IEssence
    {
        public EssenceType Type => EssenceType.Mana;

        public int EssenceTier { get; }
        
        public float EssenceContained { get; }

        public ManaEssence(int essenceTier, float essenceContained)
        {
            EssenceTier = essenceTier;
            EssenceContained = essenceContained;
        }
    }
}