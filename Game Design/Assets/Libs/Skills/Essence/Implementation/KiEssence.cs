﻿using System;

namespace Assets.Libs.Skills.Essence.Implementation
{

    [Serializable]
    public class KiEssence : IEssence
    {
        public EssenceType Type => EssenceType.Ki;

        public int EssenceTier { get; }
        
        public float EssenceContained { get; }

        public KiEssence(int essenceTier, float essenceContained)
        {
            EssenceTier = essenceTier;
            EssenceContained = essenceContained;
        }
    }
}