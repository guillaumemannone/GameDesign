﻿namespace Assets.Libs.Skills.BodyImprovement.Improvements
{
    public class Meridian : IImprovement
    {
        public ImprovementType ImprovementType => ImprovementType.Meridian;
    }
}