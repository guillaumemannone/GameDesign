using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Libs.Utils.QuadTree
{
    public abstract class QuadTreeGrid<TObject> : MonoBehaviour where TObject : MonoBehaviour, IQuadTreeObject
    {
        public QuadTreeGridNode<TObject> RootNode;
        public int MaxRecursionDepth;
        public long MinNodeRadius;
        public Transform MainCameraTransform;

        private readonly List<int> _precisionDegrees = new() {7, 3};

        public void OnMovement(Tuple<long, long> position)
        {
            if (RootNode == null) return;

            if (position.Item1 > RootNode.CenterPosition.Item1 + RootNode.Radius 
                || position.Item2 > RootNode.CenterPosition.Item2 + RootNode.Radius
                || position.Item1 < RootNode.CenterPosition.Item1 - RootNode.Radius
                || position.Item2 < RootNode.CenterPosition.Item2 - RootNode.Radius)
                IncreaseTreeSize(position);


            var node = RootNode.GetNode(position, _precisionDegrees.First());
            while (node.RecursionDepth > _precisionDegrees.First())
            {
                node.CutInFour();
                node = node.GetNode(position, _precisionDegrees.First());
            }

            if (node.RecursionDepth == 0) return;
            node = node.GetNode(position, _precisionDegrees.First());

            for (var i = 0; i < _precisionDegrees.Count - 1; i++)
            {
                var depth = _precisionDegrees[i] - _precisionDegrees[i + 1];
                node.CutInFourRecursively(depth);
                node = node.GetNode(position, _precisionDegrees[i + 1]);
            }

            node.CutInFourRecursively(_precisionDegrees.Last());
        }

        protected void InstantiateNodes()
        {
            var position = new Tuple<long, long>((long)MainCameraTransform.position.x, (long)MainCameraTransform.position.y);
            GameObject chunk = CreateRootChunk(position);

            RootNode = new QuadTreeGridNode<TObject>(position, null, 
                (long)(MinNodeRadius * Mathf.Pow(2, MaxRecursionDepth)), chunk, MaxRecursionDepth);

            var node = RootNode;
            while (node.RecursionDepth > 0)
            {
                node.CutInFour();
                node = node.GetNode(position);
            }
        }

        protected abstract void IncreaseTreeSize(Tuple<long, long> position);

        public abstract GameObject CreateRootChunk(Tuple<long, long> position);
    }
}
