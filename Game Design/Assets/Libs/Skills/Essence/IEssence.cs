﻿namespace Assets.Libs.Skills.Essence
{
    public interface IEssence
    {
        public EssenceType Type { get; }

        public int EssenceTier { get; }

        public float EssenceContained { get; }
    }
}