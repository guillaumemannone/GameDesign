﻿using Assets.Libs.Utils.QuadTree;

namespace Assets.Libs.NPC
{
    public interface INpcBehavior : IQuadTreeKey
    {
        public bool IsPlayer { get; }

        public double PlayTurn();

        public void OnDeath();
    }   
}