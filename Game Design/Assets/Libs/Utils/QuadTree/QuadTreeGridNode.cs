using System;
using UnityEngine;

namespace Assets.Libs.Utils.QuadTree
{
    public class QuadTreeGridNode<TData> where TData : MonoBehaviour, IQuadTreeObject
    {
        public QuadTreeGridNode<TData> ParentNode;

        public QuadTreeGridNode<TData> UpLeftNode;
        public QuadTreeGridNode<TData> UpRightNode;
        public QuadTreeGridNode<TData> DownLeftNode;
        public QuadTreeGridNode<TData> DownRightNode;

        public Tuple<long, long> CenterPosition;
        public long Radius;
        public int RecursionDepth;

        public GameObject Chunk;

        public QuadTreeGridNode(
            Tuple<long, long> centerPosition, 
            QuadTreeGridNode<TData> parentNode,
            long radius,
            GameObject chunk,
            int recursionDepth)
        {
            CenterPosition = centerPosition;
            ParentNode = parentNode;
            Radius = radius;

            Chunk = chunk;
            if (ParentNode != null)
            {
                Chunk.name = ParentNode.Chunk.name + $"({CenterPosition.Item1},{CenterPosition.Item2})";
            }

            RecursionDepth = recursionDepth;
        }

        public GameObject GetChunk(Tuple<long, long> position)
        {
            if (Chunk != null) return Chunk;

            if (position.Item1 > CenterPosition.Item1)
            {
                if (position.Item2 > CenterPosition.Item2)
                    return UpRightNode.GetChunk(position);
                
                return DownRightNode.GetChunk(position);
                
            }
            else
            {
                if (position.Item2 > CenterPosition.Item2)
                    return UpLeftNode.GetChunk(position);
                
                return DownLeftNode.GetChunk(position);
            }
        }

        public QuadTreeGridNode<TData> GetNode(Tuple<long, long> position, int maxRecursionDepth = 0)
        {
            if (RecursionDepth < maxRecursionDepth)
            {
                if (ParentNode != null)
                    return ParentNode.GetNode(position, maxRecursionDepth);
                return GetNode(position, maxRecursionDepth - 1);
            }
            if (RecursionDepth == maxRecursionDepth) return this;

            if (position.Item1 > CenterPosition.Item1)
            {
                if (position.Item2 > CenterPosition.Item2)
                {
                    if (UpRightNode != null) return UpRightNode.GetNode(position, maxRecursionDepth);
                }
                else
                {
                    if (DownRightNode != null) return DownRightNode.GetNode(position, maxRecursionDepth); 
                }
            }
            else
            {
                if (position.Item2 > CenterPosition.Item2)
                {
                    if (UpLeftNode != null) return UpLeftNode.GetNode(position, maxRecursionDepth); 
                }
                else
                {
                    if (DownLeftNode != null) return DownLeftNode.GetNode(position, maxRecursionDepth);
                }
            }

            return this;
        }

        public void IncreaseDepth(GameObject tile, object data)
        {
            Radius *= 2;
            RecursionDepth++;
            var chunk = Chunk.AddComponent<TData>();
            chunk.InitializeNode(data);
            chunk.GetComponent<Transform>().localScale = new Vector3(chunk.Radius * 2, chunk.Radius * 2);

            var chunks = chunk.CutInFourChunks(); 
            chunk.DestroyChunk();

            var upLeftNode = UpLeftNode;
            UpLeftNode = new QuadTreeGridNode<TData>(
                new Tuple<long, long>((CenterPosition.Item1 - (Radius / 2)), (CenterPosition.Item2 + (Radius / 2))), 
                this, (Radius / 2), chunks[0], RecursionDepth - 1);
            UpLeftNode.CutInFour();
            UpLeftNode.DownRightNode.Chunk.GetComponent<TData>().DestroyGameObject();
            UpLeftNode.DownRightNode = upLeftNode;
            UpLeftNode.DownRightNode.ParentNode = UpLeftNode;
            UpLeftNode.DownRightNode.Chunk.transform.parent = UpLeftNode.Chunk.transform;
            UpLeftNode.DownRightNode.Chunk.transform.localScale = new Vector3((float)0.5, (float)0.5);
            UpLeftNode.DownRightNode.Chunk.transform.localPosition = new Vector3((float)0.25, (float)-0.25, 0);

            var upRightNode = UpRightNode;
            UpRightNode = new QuadTreeGridNode<TData>(
                new Tuple<long, long>((CenterPosition.Item1 + (Radius / 2)), (CenterPosition.Item2 + (Radius / 2))), 
                this, (Radius / 2), chunks[1], RecursionDepth - 1);
            UpRightNode.CutInFour();
            UpRightNode.DownLeftNode.Chunk.GetComponent<TData>().DestroyGameObject();
            UpRightNode.DownLeftNode = upRightNode;
            UpRightNode.DownLeftNode.ParentNode = UpRightNode;
            UpRightNode.DownLeftNode.Chunk.transform.parent = UpRightNode.Chunk.transform;
            UpRightNode.DownLeftNode.Chunk.transform.localScale = new Vector3((float)0.5, (float)0.5);
            UpRightNode.DownLeftNode.Chunk.transform.localPosition = new Vector3((float)-0.25, (float)-0.25, 0);

            var downLeftNode = DownLeftNode;
            DownLeftNode = new QuadTreeGridNode<TData>(
                new Tuple<long, long>((CenterPosition.Item1 - (Radius / 2)), (CenterPosition.Item2 - (Radius / 2))), 
                this, (Radius / 2), chunks[2], RecursionDepth - 1);
            DownLeftNode.CutInFour();
            DownLeftNode.UpRightNode.Chunk.GetComponent<TData>().DestroyGameObject();
            DownLeftNode.UpRightNode = downLeftNode;
            DownLeftNode.UpRightNode.ParentNode = DownLeftNode;
            DownLeftNode.UpRightNode.Chunk.transform.parent = DownLeftNode.Chunk.transform;
            DownLeftNode.UpRightNode.Chunk.transform.localScale = new Vector3((float)0.5, (float)0.5);
            DownLeftNode.UpRightNode.Chunk.transform.localPosition = new Vector3((float)0.25, (float)0.25, 0);

            var downRightNode = DownRightNode;
            DownRightNode = new QuadTreeGridNode<TData>(
                new Tuple<long, long>((CenterPosition.Item1 + (Radius / 2)), (CenterPosition.Item2 - (Radius / 2))), 
                this, (Radius / 2), chunks[3], RecursionDepth - 1);
            DownRightNode.CutInFour();
            DownRightNode.UpLeftNode.Chunk.GetComponent<TData>().DestroyGameObject();
            DownRightNode.UpLeftNode = downRightNode;
            DownRightNode.UpLeftNode.ParentNode = DownRightNode;
            DownRightNode.UpLeftNode.Chunk.transform.parent = DownRightNode.Chunk.transform;
            DownRightNode.UpLeftNode.Chunk.transform.localScale = new Vector3((float)0.5, (float)0.5);
            DownRightNode.UpLeftNode.Chunk.transform.localPosition = new Vector3((float)-0.25, (float)0.25, 0);
        }

        public void CutInFourRecursively(int depth)
        {
            if (depth <= 0) return;
            if (RecursionDepth <= 0) return;
            if (Chunk.GetComponent<TData>() != null)
            {
                if (DownRightNode == null)
                    CutInFour();
            }
            
            DownRightNode.CutInFourRecursively(depth-1);
            UpRightNode.CutInFourRecursively(depth - 1);
            DownLeftNode.CutInFourRecursively(depth - 1);
            UpLeftNode.CutInFourRecursively(depth - 1);
        }

        public void CutInFour()
        {
            var chunks = Chunk.GetComponent<TData>().CutInFourChunks();

            UpLeftNode = new QuadTreeGridNode<TData>(
                new Tuple<long, long>((CenterPosition.Item1 - (Radius / 2)), (CenterPosition.Item2 + (Radius / 2))), this,
                (Radius / 2), chunks[0], RecursionDepth - 1);
        

            UpRightNode = new QuadTreeGridNode<TData>(
                new Tuple<long, long>((CenterPosition.Item1 + (Radius / 2)), (CenterPosition.Item2 + (Radius / 2))), this,
                (Radius / 2), chunks[1], RecursionDepth - 1);


            DownLeftNode = new QuadTreeGridNode<TData>(
                new Tuple<long, long>((CenterPosition.Item1 - (Radius / 2)), (CenterPosition.Item2 - (Radius / 2))), this,
                (Radius / 2), chunks[2], RecursionDepth - 1);
            

            DownRightNode = new QuadTreeGridNode<TData>(
                new Tuple<long, long>((CenterPosition.Item1 + (Radius / 2)), (CenterPosition.Item2 - (Radius / 2))), this,
                (Radius / 2), chunks[3], RecursionDepth - 1);

            Chunk.GetComponent<TData>().DestroyChunk();
        }
    }

    public interface IQuadTreeObject
    {
        long Radius { get; set; }

        public GameObject[] CutInFourChunks();

        public void InitializeNode(object data);

        public void DestroyChunk();

        public void DestroyGameObject();
    }
}