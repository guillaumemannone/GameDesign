﻿using System.Collections.Generic;
using Assets.Libs.NPC;

namespace Assets
{
    public class GameInstanceData
    {
        public SortedList<double, INpcBehavior> NpcQueue { get; set; }
    }


    public class GameInstance
    {
        public NpcManagerScript NpcManager;
            



        public void LoadGame(GameInstanceData data)
        {
            NpcManager.LoadGame(data.NpcQueue);
        }

        public GameInstanceData GameSave()
        {
            return new GameInstanceData()
            {
                NpcQueue = NpcManager.NpcQueue,
            };
        }
    }
}