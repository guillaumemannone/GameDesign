﻿using Assets.Libs.Utils.QuadTree;
using UnityEngine;

namespace Assets.Libs.Grid
{
    public class DataNode
    {
        public long Prey;
        public long Predator;
        public GameObject DefaultTile;

        public QuadTreeGridNode<BiodiversityChunk> RootNode;
    }
}